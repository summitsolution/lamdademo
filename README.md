## PRIISM New Serverless Architecture with Java8, AWS API Gateway,Lambda,DynamoDB ##


  This the Proof of Concept for our PRIISM, Angular clients call the API GateWay intern call the java8 Lamdba calls the
  DynamoDB, please see the below Diagrama.
  
  ![alt text](https://bytebucket.org/summitsolution/lamdademo/raw/fd0886632133aa440e9e84ab23676661256fc44d/architecture.jpg?token=d49739a6726c3fb6e1f76622f0daaf1822de0eca)

By using Lambda together with DynamoDB and API Gateway, there is no need to deploy or manage servers for either the application tier or database tier. 
If the front end consists of mobile devices and a web app statically hosted on Amazon S3, 
the result is a completely serverless architecture with no need to deploy or manage servers anywhere in the system, 
in either the front end or back end.

**Configuration**

1.Create a DynamoDB table with the keys and attributes mentioned below.See the class com.amazonaws.lambda.model>Patient itrs Dynamo Model class
age Number:	28
dob String:	02-06-1988	
first_name String:	Pandit	
Id Number:	601	(@DynamoDBHashKey) Primary Key
last_name String:Biradar 

Make sure that the Model class matches with Dynamo Table else you will get an error 
 Please go through this : http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Java.html?shortFooter=true
 
 2. Create Lambda functions, one for each handler in the PatientRegistrationFunctions class.please go through below link
 http://docs.aws.amazon.com/lambda/latest/dg/java-programming-model-handler-types.html
 
 PatientRegistrationFunctions class has two method one for save and one for get ,each method act as lambda fucntion
 here in aws we have choosen the Java 8 as runtime to write the lambda functions.
 
 3.Create a API Gateway API. Note that even if you don't do this step, you can still test the Lambda functions via the Lambda console "test function" tab.
  Note : Only One LambdaFunction for each Resources.
  
  Please see the below URL for API Gateway 
  http://docs.aws.amazon.com/apigateway/latest/developerguide/generate-java-sdk-of-an-api.html?shortFooter=true
  
