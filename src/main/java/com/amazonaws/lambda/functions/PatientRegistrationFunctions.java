package com.amazonaws.lambda.functions;

import java.util.List;

import org.apache.log4j.Logger;

import com.amazonaws.lambda.manager.DynamoDBClientFactory;
import com.amazonaws.lambda.model.Patient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

public class PatientRegistrationFunctions {
	
	  private static final Logger log = Logger.getLogger(PatientRegistrationFunctions.class);
	  private static final DynamoDBMapper dynmoDb = new DynamoDBClientFactory().mapper();
	  
	  public void saveOrUpdatePatient(Patient patient) {

	        if (null == patient) {
	            log.error("Savepatient received null input");
	            throw new IllegalArgumentException("Cannot save null object");
	        }

	        //log.info("Saving or updating event for team = " + patient.getHomeTeam() + " , date = " + patient.getEventDate());
	        dynmoDb.save(patient);
	        log.info("Successfully saved/updated patient");
	    }
	  
	  public List<Patient> getAllEventsHandler() {

	        log.info("GetAllEvents invoked to scan table for ALL events");
	        List<Patient> patient = dynmoDb.scan(Patient.class, new DynamoDBScanExpression());
	        log.info("Found " + patient.size() + " total patient.");
	        return patient;
	    }

	
}