package com.amazonaws.lambda.manager;

import java.io.IOException;
import java.util.Date;

import com.amazonaws.lambda.model.Patient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;

// Only for test Purpose
public class DynamoDBMapperCRUDExample {
	
	public static void main(String[] args) throws IOException {
        testCRUDOperations();
        System.out.println("Example complete!");
    }
	private static void testCRUDOperations() {

        Patient item = new Patient();
        item.setId(602);
        item.setFirst_name("Pandit");
        item.setLast_name("Biradar");
        item.setAge(28);
        item.setDob("02-06-1988");

        // Save the item (patient).
        DynamoDBClientFactory dynamoDb = new DynamoDBClientFactory();
        DynamoDBMapper mapper = dynamoDb.mapper();
        mapper.save(item);

        // Retrieve the item.
        Patient itemRetrieved = mapper.load(Patient.class, 602);
        System.out.println("Item retrieved:");
        System.out.println(itemRetrieved);

        // Update the item.
        item.setFirst_name("Pandit M");
        mapper.save(itemRetrieved);
        System.out.println("Item updated:");
        System.out.println(itemRetrieved);

        // Retrieve the updated item.
        DynamoDBMapperConfig config = new DynamoDBMapperConfig(DynamoDBMapperConfig.ConsistentReads.CONSISTENT);
        Patient updatedItem = mapper.load(Patient.class, 602, config);
        System.out.println("Retrieved the previously updated item:");
        System.out.println(updatedItem);

        // Delete the item.
       // mapper.delete(updatedItem);

        // Try to retrieve deleted item.
        Patient deletedItem = mapper.load(Patient.class, updatedItem.getId(), config);
        if (deletedItem == null) {
            System.out.println("Done - Sample item is deleted.");
        }
    }

}
