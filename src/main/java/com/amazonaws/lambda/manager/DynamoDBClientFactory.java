package com.amazonaws.lambda.manager;


import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

public class DynamoDBClientFactory {
	
	private  final  AmazonDynamoDB client =
	        AmazonDynamoDBClientBuilder.standard()
	            .withRegion(Regions.US_EAST_1)
	        //  .withCredentials(new ProfileCredentialsProvider("adminuser"))
	          .build();

	    public  DynamoDBMapper mapper() {
	        return new DynamoDBMapper(client);
	    }
	    
	    

}
