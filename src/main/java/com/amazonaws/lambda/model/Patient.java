package com.amazonaws.lambda.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.*;

@DynamoDBTable(tableName = "patient")
public class Patient {
	
	 private Integer id;
     private String first_name;
     private String last_name;
     private Integer age;
     private String dob;
    
    @DynamoDBHashKey(attributeName = "Id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@DynamoDBAttribute(attributeName = "first_name")
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	@DynamoDBAttribute(attributeName = "last_name")
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	
	@DynamoDBAttribute(attributeName = "age")
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	@DynamoDBAttribute(attributeName = "dob")
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	@Override
	public String toString() {
		return "Patient [id=" + id + ", first_name=" + first_name + ", last_name=" + last_name + ", age=" + age
				+ ", dob=" + dob + "]";
	}

}
